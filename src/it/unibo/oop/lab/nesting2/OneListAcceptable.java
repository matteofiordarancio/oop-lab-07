package it.unibo.oop.lab.nesting2;

import java.util.ArrayList;
import java.util.List;

public class OneListAcceptable<T> implements Acceptable<T> {
	private final List<T> target;



	public OneListAcceptable(final List<T> target) {
		this.target= new ArrayList<>(target);
	}



	@Override
	public Acceptor<T> acceptor() {
		return new Acceptor<T>(){
			private int i=0;
			@Override
			public void accept(final T newElement) throws ElementNotAcceptedException {
				if(i>=target.size() || target.get(i)!=newElement ) {
					throw new ElementNotAcceptedException(newElement);
				}
				i++;
			}

			@Override
			public void end() throws EndNotAcceptedException {
				if(i!=target.size()) {
					throw new EndNotAcceptedException();
				}

			}

		};
	}
}
