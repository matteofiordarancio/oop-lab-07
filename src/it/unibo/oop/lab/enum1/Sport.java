/**
 *
 */
package it.unibo.oop.lab.enum1;

/**
 * Represents an enumeration for declaring sports;
 *
 * 1) Complete the definition of the enumeration.
 *
 */
public enum Sport {
	Basket, Soccer, Tennis, Bike, F1, VOLLEY, MOTOGP;
}
