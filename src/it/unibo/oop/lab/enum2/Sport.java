/**
 *
 */
package it.unibo.oop.lab.enum2;

import static it.unibo.oop.lab.enum2.Place.INDOOR;
import static it.unibo.oop.lab.enum2.Place.OUTDOOR;;

/**
 * Represents an enumeration for declaring sports.
 *
 * 1) You must add a field keeping track of the number of members each team is
 * composed of (1 for individual sports)
 *
 * 2) A second field will keep track of the name of the sport.
 *
 * 3) A third field, of type Place, will allow to define if the sport is
 * practiced indoor or outdoor
 *
 */
public enum Sport {
	BASKET(INDOOR, 5, "Basket"),
	VOLLEY(INDOOR, 6, "Volley"),
	TENNIS(OUTDOOR, 1, "Tennis"),
	BIKE(OUTDOOR, 1, "Bike"),
	F1(OUTDOOR, 1, "Formula 1"),
	MOTOGP(OUTDOOR, 1, "Moto GP"),
	SOCCER(OUTDOOR, 11, "Soccer");

	private final Place place;
	private final int noTeamMembers;
	private final String actualName;

	/*
	 * TODO
	 *
	 * [CONSTRUCTOR]
	 *
	 * Define a constructor like this:
	 */
	Sport(final Place place, final int noTeamMembers, final String actualName) {
		this.place = place;
		this.noTeamMembers = noTeamMembers;
		this.actualName = actualName;
	}

	public boolean isIndividualSport() {
		return noTeamMembers == 1;
	}

	public boolean isIndoorSport() {
		return place == INDOOR;
	}

	public Place getPlace() {
		return place;
	}

	@Override
	public String toString() {
		return "Sport: " + actualName + ". Currently played " + place + " in a team of " + noTeamMembers
				+ " member(s).";
	}
}
